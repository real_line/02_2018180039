#pragma once
#include "Renderer.h"
#include "GSEObject.h"
#include "GSEGlobals.h"
#include"Sound.h"

struct Enemy {
	int m_EnemyID = -1;
	int m_GunID = -1;
	int m_BulletID = -1;
	int enemySpriteY = 0;
	int gunSpriteX = 0;
	bool isCollideBulletSword = false;
};

class GSEGame
{
public:
	GSEGame();
	~GSEGame();

	void RenderScene();
	int AddObject(float x, float y, float depth,
		float sx, float sy,
		float velX, float velY,
		float accX, float accY,
		float mass);
	void DeleteObject(int index);
	void Update(float elapsedTimeInSec, GSEInputs* inputs);
private:
	void init();
	bool AABBCollision(GSEObject* a, GSEObject* b);
	bool ProcessCollision(GSEObject* a, GSEObject* b);
	void AdjustPosition(GSEObject* a, GSEObject* b);
	void DoGarbageCollect();
	void ObjectClear();

	Renderer* m_renderer = NULL;
	Sound* m_Sound = NULL;
	GSEObject* m_Objects[GSE_MAX_OBJECTS];
	GSEGameScene currentScene = GSEGameScene::TITLE;

	vector<Enemy> enemy;

	int m_EndID = -1;
	int m_HeroID = -1;
	int heroSpriteY = 0;

	int m_titleLogoTexture = -1;
	int m_GameClearTexture = -1;

	int m_SpriteTexture = - 1;
	int m_BgTexture = -1;
	int m_EnemyTexture = -1;
	int m_GunTexture = -1;
	int m_BulletTexture = -1;
	int m_SwordEffectTexture = -1;

	int m_SnowParticle = -1;

	int m_BgSound = -1;
	int m_SwordSound = -1;
};