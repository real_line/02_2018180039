#include "stdafx.h"
#include "GSEGame.h"
#include "math.h"

float characterAnimTime = 0;
float swordAnimTime = 0;

float testTime = 0;

GSEGame::GSEGame()
{
	//Renderer initialize
	m_renderer = new Renderer(GSE_WINDOWSIZE_X, GSE_WINDOWSIZE_Y);

	m_SpriteTexture = m_renderer->GenPngTexture("character.png");
	m_BgTexture = m_renderer->GenPngTexture("bg.png");
	m_EnemyTexture = m_renderer->GenPngTexture("enemy.png");
	m_GunTexture = m_renderer->GenPngTexture("gun.png");
	m_BulletTexture = m_renderer->GenPngTexture("bullet.png");
	m_titleLogoTexture = m_renderer->GenPngTexture("title.png");
	m_GameClearTexture = m_renderer->GenPngTexture("gameClear.png");
	m_SwordEffectTexture = m_renderer->GenPngTexture("effect.png");

	m_Sound = new Sound();

	m_BgSound = m_Sound->CreateBGSound("Suspect.mp3");
	m_Sound->PlayBGSound(m_BgSound, true, 1.f);

	m_SwordSound = m_Sound->CreateShortSound("sword.mp3");
}

GSEGame::~GSEGame()
{
	//Renderer delete
	m_Sound->DeleteBGSound(m_BgSound);
	m_Sound->DeleteShortSound(m_SwordSound);

	for (int i = 0; i < GSE_MAX_OBJECTS; ++i)
		DeleteObject(i);

	delete m_renderer;
	delete m_Sound;
}

void GSEGame::Update(float elapsedTimeInSec, GSEInputs* inputs)
{
	//do garbage collecting
	DoGarbageCollect();

	switch (currentScene)
	{
	case TITLE: {
		if (inputs->KEY_A ==true) {
			ObjectClear();
			init();
			currentScene = GSEGameScene::GAME;
		}
		break;
	}
	case GAME: {
		GSEUpdateParams othersParam;
		GSEUpdateParams heroParam;
		memset(&othersParam, 0, sizeof(GSEUpdateParams));
		memset(&heroParam, 0, sizeof(GSEUpdateParams));

		//Processing collision
		bool isCollide[GSE_MAX_OBJECTS];
		memset(isCollide, 0, sizeof(bool) * GSE_MAX_OBJECTS);
		for (int i = 0; i < GSE_MAX_OBJECTS; i++)
		{
			for (int j = i + 1; j < GSE_MAX_OBJECTS; j++)
			{
				if (m_Objects[i] != NULL && m_Objects[j] != NULL)
				{
					bool collide = ProcessCollision(m_Objects[i], m_Objects[j]);
					if (collide)
					{
						isCollide[i] = true;
						isCollide[j] = true;
					}
				}
			}
		}

		for (int i = 0; i < GSE_MAX_OBJECTS; i++)
		{
			if (m_Objects[i] != NULL)
			{
				if (!isCollide[i])
					m_Objects[i]->SetState(GSEObjectState::STATE_FALLING);
			}
		}


		//calc force
		float forceAmount = 500.f;
		if (inputs->KEY_W)
		{
			heroParam.forceY += 12 * forceAmount;
		}
		if (inputs->KEY_A)
		{
			heroParam.forceX -= forceAmount;
			heroSpriteY = 1;
		}
		if (inputs->KEY_S)
		{
			heroParam.forceY -= forceAmount;
			heroSpriteY = 0;
		}
		if (inputs->KEY_D)
		{
			heroParam.forceX += forceAmount;
			heroSpriteY = 2;
		}
		if (!(inputs->KEY_W || inputs->KEY_A || inputs->KEY_S || inputs->KEY_D)) {
			heroSpriteY = 0;
		}

		//Update All Objects
		for (int i = 0; i < GSE_MAX_OBJECTS; i++)
		{
			if (m_Objects[i] != NULL)
			{
				if (i == m_HeroID)
				{
					m_Objects[i]->Update(elapsedTimeInSec, &heroParam);
				}
				else
				{
					if (m_Objects[i]->GetStickToParent())
					{
						float posX, posY, depth;
						float relPosX, relPosY, relDepth;
						int parentID = m_Objects[i]->GetParentID();
						if (m_Objects[parentID] == NULL) {
							DeleteObject(i);
							continue;
						}

						m_Objects[parentID]->GetPosition(&posX, &posY, &depth);
						m_Objects[i]->GetRelPosition(&relPosX, &relPosY, &relDepth);
						m_Objects[i]->SetPosition(posX + relPosX, posY + relPosY, depth + relDepth);
						m_Objects[i]->Update(elapsedTimeInSec, &othersParam);
					}
					else
					{
						m_Objects[i]->Update(elapsedTimeInSec, &othersParam);
					}
				}
			}
		}

		//sword
		float swordPosX = 0.f;
		float swordPosY = 0.f;

		if (inputs->ARROW_LEFT) swordPosX += -1.f;

		if (inputs->ARROW_RIGHT) swordPosX += 1.f;

		if (inputs->ARROW_DOWN) swordPosY += -1.f;

		if (inputs->ARROW_UP) swordPosY += 1.f;


		float swordDirSize = sqrtf(swordPosX * swordPosX + swordPosY * swordPosY);
		if (swordDirSize > 0.f)
		{
			float norDirX = swordPosX / swordDirSize;
			float norDirY = swordPosY / swordDirSize;

			float aX, aY, asX, asY;
			float bX, bY, bsX, bsY;
			float temp;

			// hero id, size get
			m_Objects[m_HeroID]->GetPosition(&aX, &aY, &temp);
			m_Objects[m_HeroID]->GetSize(&asX, &asY);

			if (m_Objects[m_HeroID]->GetRemainingCoolTime() < 0.f)
			{
				// ������Ʈ ����
				int effect = AddObject(0.f, 0.f, 0.f, 1.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f);
				m_Objects[effect]->SetParentID(m_HeroID);
				m_Objects[effect]->SetRelPosition(norDirX / 1.5f, norDirY / 1.5f, 0.f);
				m_Objects[effect]->SetStickToParent(true);
				m_Objects[effect]->SetType(GSEObjectType::TYPE_SWORD);
				m_Objects[effect]->SetLife(100.f);
				m_Objects[effect]->SetLifeTime(0.2f);
				m_Objects[effect]->SetTextureID(m_SwordEffectTexture);

				m_Sound->PlayShortSound(m_SwordSound, false, 1.f);

				// ��Ÿ�� ����
				m_Objects[m_HeroID]->ResetRemainingCoolTime();
			}
		}

		// enemy, bullet update
		for (int i = 0; i < enemy.size(); ++i) {
			if (enemy[i].m_EnemyID == -1)
				continue;

			if (m_Objects[enemy[i].m_EnemyID] == NULL)
				continue;

			GSEObjectType type;
			m_Objects[enemy[i].m_EnemyID]->GetType(&type);

			if (type != GSEObjectType::TYPE_MOVABLE)
				continue;

			float heroX, heroY, heroDepth;
			float enemyX, enemyY, enemyDepth;

			m_Objects[m_HeroID]->GetPosition(&heroX, &heroY, &heroDepth);
			m_Objects[enemy[i].m_EnemyID]->GetPosition(&enemyX, &enemyY, &enemyDepth);

			if (heroX > enemyX - 4.f && heroX < enemyX + 4.f) {
				if (heroX < enemyX) {
					enemy[i].enemySpriteY = 1;
					enemy[i].gunSpriteX = 1;
					m_Objects[enemy[i].m_GunID]->SetRelPosition(-0.5f, 0.f, 0.f);
				}
				else {
					enemy[i].enemySpriteY = 2;
					enemy[i].gunSpriteX = 0;
					m_Objects[enemy[i].m_GunID]->SetRelPosition(0.5f, 0.f, 0.f);
				}
				m_Objects[enemy[i].m_EnemyID]->SetState(GSEObjectState::STATE_CHASING);
			}
			else {
				m_Objects[enemy[i].m_EnemyID]->SetState(GSEObjectState::STATE_FALLING);
			}

			GSEObjectState state;
			m_Objects[enemy[i].m_EnemyID]->GetState(&state);

			if (state == GSEObjectState::STATE_CHASING) {
				if (m_Objects[enemy[i].m_EnemyID]->GetRemainingCoolTime() < 0.f) {
					enemy[i].isCollideBulletSword = false;
					// ������Ʈ ����
					enemy[i].m_BulletID = AddObject(0.f, 0.f, 0.f, 0.3f, 0.3f, 0.f, 0.f, 0.f, 0.f, 1.f);
					// parent ����
					m_Objects[enemy[i].m_BulletID]->SetParentID(enemy[i].m_EnemyID);
					// ��� ��ǥ
					m_Objects[enemy[i].m_BulletID]->SetRelPosition(0.f, 0.f, 0.f);
					// true�� ��� ��ǥ ��� -> physics ��, false�� �׳� ������
					m_Objects[enemy[i].m_BulletID]->SetStickToParent(true);
					m_Objects[enemy[i].m_BulletID]->SetType(GSEObjectType::TYPE_BULLET);
					m_Objects[enemy[i].m_BulletID]->SetLife(100.f);
					m_Objects[enemy[i].m_BulletID]->SetLifeTime(1.5f);
					m_Objects[enemy[i].m_BulletID]->SetTextureID(m_BulletTexture);

					m_Objects[enemy[i].m_EnemyID]->ResetRemainingCoolTime();
				}
			}
			else {
				testTime += 0.01f;
				m_Objects[enemy[i].m_EnemyID]->SetPosition(enemyX + sin(testTime) / 40.f, enemyY, enemyDepth);
				if (sin(testTime) < 0) {
					enemy[i].enemySpriteY = 1;
					enemy[i].gunSpriteX = 1;
					m_Objects[enemy[i].m_GunID]->SetRelPosition(-0.5f, 0.f, 0.f);
				}
				else {
					enemy[i].enemySpriteY = 2;
					enemy[i].gunSpriteX = 0;
					m_Objects[enemy[i].m_GunID]->SetRelPosition(0.5f, 0.f, 0.f);
				}
			}

			if (enemy[i].m_BulletID == -1)
				continue;

			if (m_Objects[enemy[i].m_BulletID] == NULL)
				continue;

			if (enemy[i].m_EnemyID != m_Objects[enemy[i].m_BulletID]->GetParentID())
				continue;

			m_Objects[enemy[i].m_BulletID]->GetType(&type);

			if (type == GSEObjectType::TYPE_BULLET) {
				float rx, ry, rdepth;
				m_Objects[enemy[i].m_BulletID]->GetRelPosition(&rx, &ry, &rdepth);
	
				if (heroX < enemyX) {
					if (enemy[i].isCollideBulletSword)
						m_Objects[enemy[i].m_BulletID]->SetRelPosition(rx + 0.1f, ry, rdepth);
					else {
						m_Objects[enemy[i].m_BulletID]->SetRelPosition(rx - 0.1f, ry, rdepth);
					}
				}
				else {
					if (enemy[i].isCollideBulletSword)
						m_Objects[enemy[i].m_BulletID]->SetRelPosition(rx - 0.1f, ry, rdepth);
					else
						m_Objects[enemy[i].m_BulletID]->SetRelPosition(rx + 0.1f, ry, rdepth);
				}
			}
		}

		float x, y, z;
		m_Objects[m_HeroID]->GetPosition(&x, &y, &z);
		m_renderer->SetCameraPos(x * 100.f, y * 100.f);
		break;
	}
	case END: {
		m_renderer->SetCameraPos(0, 0);
		if (inputs->KEY_A == true) {
			inputs->KEY_A = false;
			currentScene = GSEGameScene::TITLE;
		}
		break;
	}
	default:
		break;
	}
}

bool GSEGame::ProcessCollision(GSEObject* a, GSEObject* b)
{
	GSEObjectType aType, bType;
	a->GetType(&aType);
	b->GetType(&bType);

	if (aType == GSEObjectType::TYPE_HERO && bType == GSEObjectType::TYPE_SWORD)
		return 0;
	if (bType == GSEObjectType::TYPE_HERO && aType == GSEObjectType::TYPE_SWORD)
		return 0;

	if (aType == GSEObjectType::TYPE_HERO && bType == GSEObjectType::TYPE_MOVABLE)
		return 0;
	if (bType == GSEObjectType::TYPE_HERO && aType == GSEObjectType::TYPE_MOVABLE)
		return 0;

	if (aType == GSEObjectType::TYPE_HERO && bType == GSEObjectType::TYPE_GUN)
		return 0;
	if (bType == GSEObjectType::TYPE_HERO && aType == GSEObjectType::TYPE_GUN)
		return 0;

	bool isCollide = AABBCollision(a, b);
	if (isCollide)
	{
		//do something
		// bullet and Hero
		if (aType == GSEObjectType::TYPE_BULLET && bType == GSEObjectType::TYPE_HERO)
		{
			ObjectClear();
			init();
		}
		if (bType == GSEObjectType::TYPE_BULLET && aType == GSEObjectType::TYPE_HERO)
		{
			ObjectClear();
			init();
		}

		// sword and enemy
		if (aType == GSEObjectType::TYPE_SWORD && bType == GSEObjectType::TYPE_MOVABLE)
		{
			b->SetLife(-1);
		}
		if (bType == GSEObjectType::TYPE_SWORD && aType == GSEObjectType::TYPE_MOVABLE)
		{
			a->SetLife(-1);
		}

		// sword and bullet
		if (aType == GSEObjectType::TYPE_SWORD && bType == GSEObjectType::TYPE_BULLET)
		{
			for (int i = 0; i < enemy.size(); ++i) {
				if (enemy[i].m_EnemyID == b->GetParentID()) {
					enemy[i].isCollideBulletSword = true;
					break;
				}
			}
		}

		if (bType == GSEObjectType::TYPE_SWORD && aType == GSEObjectType::TYPE_BULLET)
		{
			for (int i = 0; i < enemy.size(); ++i) {
				if (enemy[i].m_EnemyID == a->GetParentID()) {
					enemy[i].isCollideBulletSword = true;
					break;
				}
			}
		}

		// enemy and bullet
		if (aType == GSEObjectType::TYPE_MOVABLE && bType == GSEObjectType::TYPE_BULLET)
		{
			for (auto i = enemy.begin(); i < enemy.end(); ++i) {
				if ((*i).m_EnemyID == b->GetParentID()) {
					if ((*i).isCollideBulletSword) {
						(*i).isCollideBulletSword = false;
						enemy.erase(i);
						a->SetLife(-1);
						break;
					}
				}
			}
		}
		
		if (bType == GSEObjectType::TYPE_MOVABLE && aType == GSEObjectType::TYPE_BULLET)
		{
			for (auto i = enemy.begin(); i < enemy.end(); ++i) {
				if ((*i).m_EnemyID == a->GetParentID()) {
					if ((*i).isCollideBulletSword) {
						(*i).isCollideBulletSword = false;
						enemy.erase(i);
						b->SetLife(-1);
						break;
					}
				}
			}
		}	

		// m_EndID
		if (aType == GSEObjectType::TYPE_HERO && bType == GSEObjectType::TYPE_END) {
			currentScene = GSEGameScene::END;
		}
		

		if (bType == GSEObjectType::TYPE_HERO && aType == GSEObjectType::TYPE_END) {
			currentScene = GSEGameScene::END;
		}
		
	}
	return isCollide;
}

bool GSEGame::AABBCollision(GSEObject* a, GSEObject* b)
{
	GSEObjectType aType;
	GSEObjectType bType;

	float aMinX, aMaxX, aMinY, aMaxY;
	float bMinX, bMaxX, bMinY, bMaxY;
	float aX, aY, asX, asY;
	float bX, bY, bsX, bsY;
	float temp;

	a->GetType(&aType);
	b->GetType(&bType);

	a->GetPosition(&aX, &aY, &temp);
	a->GetSize(&asX, &asY);
	b->GetPosition(&bX, &bY, &temp);
	b->GetSize(&bsX, &bsY);

	aMinX = aX - asX / 2.f;
	aMaxX = aX + asX / 2.f;
	aMinY = aY - asY / 2.f;
	aMaxY = aY + asY / 2.f;
	bMinX = bX - bsX / 2.f;
	bMaxX = bX + bsX / 2.f;
	bMinY = bY - bsY / 2.f;
	bMaxY = bY + bsY / 2.f;

	if (aMinX > bMaxX) // || fabs(aMinX-bMaxX)<FLT_EPSILON
	{
		return false;
	}
	if (aMaxX < bMinX)
	{
		return false;
	}
	if (aMinY > bMaxY)
	{
		return false;
	}
	if (aMaxY < bMinY)
	{
		return false;
	}

	AdjustPosition(a, b);
	return true;
}

void GSEGame::AdjustPosition(GSEObject* a, GSEObject* b)
{
	GSEObjectType aType;
	GSEObjectType bType;

	float aMinX, aMaxX, aMinY, aMaxY;
	float bMinX, bMaxX, bMinY, bMaxY;
	float aX, aY, asX, asY;
	float bX, bY, bsX, bsY;
	float temp;

	a->GetType(&aType);
	b->GetType(&bType);

	a->GetPosition(&aX, &aY, &temp);
	a->GetSize(&asX, &asY);
	b->GetPosition(&bX, &bY, &temp);
	b->GetSize(&bsX, &bsY);

	aMinX = aX - asX / 2.f;
	aMaxX = aX + asX / 2.f;
	aMinY = aY - asY / 2.f;
	aMaxY = aY + asY / 2.f;
	bMinX = bX - bsX / 2.f;
	bMaxX = bX + bsX / 2.f;
	bMinY = bY - bsY / 2.f;
	bMaxY = bY + bsY / 2.f;

	if ((aType == GSEObjectType::TYPE_MOVABLE || aType == GSEObjectType::TYPE_HERO)
		&&
		bType == GSEObjectType::TYPE_FIXED)
	{
		if (aMaxY< bMaxY && aMinY>bMinY) {
			float vx, vy;

			if (aMinX > bMinX) {
				a->GetVel(&vx, &vy);
				a->SetVel(1.f, -1.f);
			}
			else {
				a->GetVel(&vx, &vy);
				a->SetVel(-1.f, -1.f);
			}
			a->SetState(GSEObjectState::STATE_GROUND);
			b->SetState(GSEObjectState::STATE_GROUND);
			return;
		}
		if (aMaxY > bMaxY)
		{
			aY = aY + (bMaxY - aMinY);

			a->SetPosition(aX, aY, 0.f);

			float vx, vy;
			a->GetVel(&vx, &vy);
			a->SetVel(vx, 0.f);

			a->SetState(GSEObjectState::STATE_GROUND);
			b->SetState(GSEObjectState::STATE_GROUND);
		}
		else
		{
			aY = aY - (aMaxY - bMinY);

			a->SetPosition(aX, aY, 0.f);

			float vx, vy;
			a->GetVel(&vx, &vy);
			a->SetVel(vx, 0.f);

			a->SetState(GSEObjectState::STATE_FALLING);
			b->SetState(GSEObjectState::STATE_GROUND);
		}
	}
	else if (
		(bType == GSEObjectType::TYPE_MOVABLE || bType == GSEObjectType::TYPE_HERO)
		&&
		(aType == GSEObjectType::TYPE_FIXED)
		)
	{
		if (!(bMaxY > aMaxY && bMinY < aMinY))
		{
			if (bMaxY > aMaxY)
			{
				bY = bY + (aMaxY - bMinY);

				b->SetPosition(bX, bY, 0.f);
				float vx, vy;
				b->GetVel(&vx, &vy);
				b->SetVel(vx, 0.f);

				a->SetState(GSEObjectState::STATE_GROUND);
				b->SetState(GSEObjectState::STATE_GROUND);
			}
			else
			{
				bY = bY - (bMaxY - aMinY);

				b->SetPosition(bX, bY, 0.f);
				float vx, vy;
				b->GetVel(&vx, &vy);
				b->SetVel(vx, 0.f);

				a->SetState(GSEObjectState::STATE_GROUND);
				b->SetState(GSEObjectState::STATE_FALLING);
			}
		}
	}
}

void GSEGame::DoGarbageCollect()
{
	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		if (m_Objects[i] != NULL)
		{
			float life = m_Objects[i]->GetLife();
			float lifeTime = m_Objects[i]->GetLifeTime();
			if (life < 0.f || lifeTime < 0.f)
			{
				DeleteObject(i);
			}
		}
	}
}

void GSEGame::RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	switch (currentScene)
	{
	case TITLE: {
		// background
		m_renderer->DrawGround(0, 0, 0, 1000, 600, 1, 1, 1, 1, 1, m_titleLogoTexture);
		break;
	}
	case GAME: {
		// background
		m_renderer->DrawGround(0, 0, 0, 3820, 2162, 1, 1, 1, 1, 1, m_BgTexture);

		//Draw All Objects
		for (int i = 0; i < GSE_MAX_OBJECTS; i++)
		{
			if (m_Objects[i] != NULL)
			{
				float x, y, depth;
				m_Objects[i]->GetPosition(&x, &y, &depth);
				float sx, sy;
				m_Objects[i]->GetSize(&sx, &sy);

				int textureID = m_Objects[i]->GetTextureID();

				//meter to pixel
				x = x * 100.f;
				y = y * 100.f;
				sx = sx * 100.f;
				sy = sy * 100.f;

				if (textureID < 0) {
					//m_renderer->DrawSolidRectGauge(x, y, depth, 1, 1, 1, sx, sy, 0.f, 1, 0, 1, 1, 50, 0);
					m_renderer->DrawSolidRect(x, y, depth, sx, sy, 0.f, 0, 0, 0, 0);

				}
				else {
					GSEObjectType type;
					m_Objects[i]->GetType(&type);
					if (type == GSEObjectType::TYPE_HERO) {
						m_renderer->DrawTextureRectAnim(
							x, y, depth,
							sx, sy, 1.f,
							1.f, 1.f, 1.f, 1.f,
							textureID,
							4,
							4,
							(int)characterAnimTime % 4,	//x
							heroSpriteY		//y
						);
					}
					if (type == GSEObjectType::TYPE_SWORD) {
						m_renderer->DrawTextureRectAnim(
							x, y, depth,
							sx, sy, 1.f,
							1.f, 1.f, 1.f, 1.f,
							textureID,
							5,
							5,
							(int)swordAnimTime % 5,	//x
							1		//y
						);
					}
					if (type == GSEObjectType::TYPE_MOVABLE) {

						for (int n = 0; n < enemy.size(); ++n) {
							if (enemy[n].m_EnemyID == i) {
								m_renderer->DrawTextureRectAnim(
									x, y, depth,
									sx, sy, 1.f,
									1.f, 1.f, 1.f, 1.f,
									textureID,
									4,
									4,
									(int)characterAnimTime % 4,	//x
									enemy[n].enemySpriteY				//y
								);
								break;
							}
						}
					}
					if (type == GSEObjectType::TYPE_GUN) {
						for (int n = 0; n < enemy.size(); ++n) {
							if (enemy[n].m_GunID == i) {
								m_renderer->DrawTextureRectAnim(
									x, y, depth,
									sx, sy, 1.f,
									1.f, 1.f, 1.f, 1.f,
									textureID,
									2,
									1,
									enemy[n].gunSpriteX,
									0
								);
								break;
							}
						}

					}
					if (type == GSEObjectType::TYPE_BULLET) {
						m_renderer->DrawTextureRect(
							x, y, depth,
							sx, sy, 1.f,
							1.f, 1.f, 1.f, 1.f,
							textureID
						);
					}
				}
			}
		}

		characterAnimTime += 0.1f;
		swordAnimTime += 0.3f;
		break;
	}
	case END: {
		m_renderer->DrawGround(0, 0, 0, 1000, 600, 1, 1, 1, 1, 1, m_GameClearTexture);
		break;
	}
	default:
		break;
	}
}

int GSEGame::AddObject(float x, float y, float depth,
	float sx, float sy,
	float velX, float velY,
	float accX, float accY,
	float mass)
{
	//find empty slot
	int index = -1;
	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		if (m_Objects[i] == NULL)
		{
			index = i;
			break;
		}
	}

	if (index < 0)
	{
		std::cout << "No empty object slot.. " << std::endl;
		return -1;
	}

	m_Objects[index] = new GSEObject();
	m_Objects[index]->SetPosition(x, y, depth);
	m_Objects[index]->SetSize(sx, sy);
	m_Objects[index]->SetVel(velX, velY);
	m_Objects[index]->SetAcc(accX, accY);
	m_Objects[index]->SetMass(mass);

	return index;
}

void GSEGame::DeleteObject(int index)
{
	if (m_Objects[index] != NULL)
	{
		delete m_Objects[index];
		m_Objects[index] = NULL;
	}
	else
	{
		std::cout << "Try to delete NULL object : " << index << std::endl;
	}
}

void GSEGame::ObjectClear()
{
	characterAnimTime = 0;
	swordAnimTime = 0;
	testTime = 0;
	enemy.clear();


	for (int i = 0; i < GSE_MAX_OBJECTS; ++i) {
		if (m_Objects[i] != NULL)
			DeleteObject(i);
	}
}


void GSEGame::init()
{
	for (int i = 0; i < GSE_MAX_OBJECTS; ++i)
		m_Objects[i] = NULL;

	//Create Hero
	m_HeroID = AddObject(-16.5f, 2.f, 0.f, 1.f, 1.f, 0.f, 0.f, 0.f, 10.f, 20.f);
	m_Objects[m_HeroID]->SetType(GSEObjectType::TYPE_HERO);
	m_Objects[m_HeroID]->SetApplyPhysics(true);
	m_Objects[m_HeroID]->SetLife(10000000.f);
	m_Objects[m_HeroID]->SetLifeTime(10000000.f);
	m_Objects[m_HeroID]->SetTextureID(m_SpriteTexture);


	// enemy0
	Enemy tmpEnemy;
	tmpEnemy.m_EnemyID = AddObject(-12.f, 2.f, 0.f, 1.f, 1.f, 0.f, 0.f, 0.f, 0.f, 20.f);
	m_Objects[tmpEnemy.m_EnemyID]->SetType(GSEObjectType::TYPE_MOVABLE);
	m_Objects[tmpEnemy.m_EnemyID]->SetApplyPhysics(true);
	m_Objects[tmpEnemy.m_EnemyID]->SetLife(1.f);
	m_Objects[tmpEnemy.m_EnemyID]->SetLifeTime(10000000.f);
	m_Objects[tmpEnemy.m_EnemyID]->SetTextureID(m_EnemyTexture);
	m_Objects[tmpEnemy.m_EnemyID]->SetCoolTime(5.f);

	tmpEnemy.m_GunID = AddObject(0.f, 0.f, 0.f, 0.3f, 0.3f, 0.f, 0.f, 0.f, 0.f, 1.f);
	m_Objects[tmpEnemy.m_GunID]->SetParentID(tmpEnemy.m_EnemyID);
	m_Objects[tmpEnemy.m_GunID]->SetRelPosition(0.5, 0, 0.f);
	m_Objects[tmpEnemy.m_GunID]->SetStickToParent(true);
	m_Objects[tmpEnemy.m_GunID]->SetType(GSEObjectType::TYPE_GUN);
	m_Objects[tmpEnemy.m_GunID]->SetLife(1000.f);
	m_Objects[tmpEnemy.m_GunID]->SetLifeTime(10000000.f);
	m_Objects[tmpEnemy.m_GunID]->SetTextureID(m_GunTexture);
	enemy.emplace_back(tmpEnemy);


	// enemy1
	tmpEnemy.m_EnemyID = AddObject(5.f, 5.f, 0.f, 1.f, 1.f, 0.f, 0.f, 0.f, 0.f, 20.f);
	m_Objects[tmpEnemy.m_EnemyID]->SetType(GSEObjectType::TYPE_MOVABLE);
	m_Objects[tmpEnemy.m_EnemyID]->SetApplyPhysics(true);
	m_Objects[tmpEnemy.m_EnemyID]->SetLife(1.f);
	m_Objects[tmpEnemy.m_EnemyID]->SetLifeTime(10000000.f);
	m_Objects[tmpEnemy.m_EnemyID]->SetTextureID(m_EnemyTexture);
	m_Objects[tmpEnemy.m_EnemyID]->SetCoolTime(5.f);

	tmpEnemy.m_GunID = AddObject(0.f, 0.f, 0.f, 0.3f, 0.3f, 0.f, 0.f, 0.f, 0.f, 1.f);
	m_Objects[tmpEnemy.m_GunID]->SetParentID(tmpEnemy.m_EnemyID);
	m_Objects[tmpEnemy.m_GunID]->SetRelPosition(0.5, 0, 0.f);
	m_Objects[tmpEnemy.m_GunID]->SetStickToParent(true);
	m_Objects[tmpEnemy.m_GunID]->SetType(GSEObjectType::TYPE_GUN);
	m_Objects[tmpEnemy.m_GunID]->SetLife(1000.f);
	m_Objects[tmpEnemy.m_GunID]->SetLifeTime(10000000.f);
	m_Objects[tmpEnemy.m_GunID]->SetTextureID(m_GunTexture);
	enemy.emplace_back(tmpEnemy);


	// enemy2
	tmpEnemy.m_EnemyID = AddObject(15.f, 5.f, 0.f, 1.f, 1.f, 0.f, 0.f, 0.f, 0.f, 20.f);
	m_Objects[tmpEnemy.m_EnemyID]->SetType(GSEObjectType::TYPE_MOVABLE);
	m_Objects[tmpEnemy.m_EnemyID]->SetApplyPhysics(true);
	m_Objects[tmpEnemy.m_EnemyID]->SetLife(1.f);
	m_Objects[tmpEnemy.m_EnemyID]->SetLifeTime(10000000.f);
	m_Objects[tmpEnemy.m_EnemyID]->SetTextureID(m_EnemyTexture);
	m_Objects[tmpEnemy.m_EnemyID]->SetCoolTime(5.f);

	tmpEnemy.m_GunID = AddObject(0.f, 0.f, 0.f, 0.3f, 0.3f, 0.f, 0.f, 0.f, 0.f, 1.f);
	m_Objects[tmpEnemy.m_GunID]->SetParentID(tmpEnemy.m_EnemyID);
	m_Objects[tmpEnemy.m_GunID]->SetRelPosition(0.5, 0, 0.f);
	m_Objects[tmpEnemy.m_GunID]->SetStickToParent(true);
	m_Objects[tmpEnemy.m_GunID]->SetType(GSEObjectType::TYPE_GUN);
	m_Objects[tmpEnemy.m_GunID]->SetLife(1000.f);
	m_Objects[tmpEnemy.m_GunID]->SetLifeTime(10000000.f);
	m_Objects[tmpEnemy.m_GunID]->SetTextureID(m_GunTexture);
	enemy.emplace_back(tmpEnemy);


	// enemy3
	tmpEnemy.m_EnemyID = AddObject(-3.f, 5.f, 0.f, 1.f, 1.f, 0.f, 0.f, 0.f, 0.f, 20.f);
	m_Objects[tmpEnemy.m_EnemyID]->SetType(GSEObjectType::TYPE_MOVABLE);
	m_Objects[tmpEnemy.m_EnemyID]->SetApplyPhysics(true);
	m_Objects[tmpEnemy.m_EnemyID]->SetLife(1.f);
	m_Objects[tmpEnemy.m_EnemyID]->SetLifeTime(10000000.f);
	m_Objects[tmpEnemy.m_EnemyID]->SetTextureID(m_EnemyTexture);
	m_Objects[tmpEnemy.m_EnemyID]->SetCoolTime(5.f);

	tmpEnemy.m_GunID = AddObject(0.f, 0.f, 0.f, 0.3f, 0.3f, 0.f, 0.f, 0.f, 0.f, 1.f);
	m_Objects[tmpEnemy.m_GunID]->SetParentID(tmpEnemy.m_EnemyID);
	m_Objects[tmpEnemy.m_GunID]->SetRelPosition(0.5, 0, 0.f);
	m_Objects[tmpEnemy.m_GunID]->SetStickToParent(true);
	m_Objects[tmpEnemy.m_GunID]->SetType(GSEObjectType::TYPE_GUN);
	m_Objects[tmpEnemy.m_GunID]->SetLife(1000.f);
	m_Objects[tmpEnemy.m_GunID]->SetLifeTime(10000000.f);
	m_Objects[tmpEnemy.m_GunID]->SetTextureID(m_GunTexture);
	enemy.emplace_back(tmpEnemy);


	// �ٴ�
	int floor = AddObject(-4.5f, -3.7f, 0.f, 26.f, 0.3f, 0.f, 0.f, 0.f, 0.f, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);

	// �ٴ� 1
	floor = AddObject(-0.3f, -2.9f, 0.f, 1.3f, 0.3f, 0.f, 0.f, 0.f, 0.f, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);

	floor = AddObject(-1.f, -3.2f, 0.f, 1.f, 0.3f, 0.f, 0.f, 0.f, 0.f, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);

	// �ٴ� 2
	floor = AddObject(9.3f, -2.9f, 0.f, 1.3f, 0.3f, 0.f, 0.f, 0.f, 0.f, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);

	floor = AddObject(8.5, -3.2, 0, 1, 0.3, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);

	// �ٴ� 3
	floor = AddObject(11.f, -0.7f, 0.f, 1.3f, 0.3f, 0.f, 0.f, 0.f, 0.f, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);

	floor = AddObject(11.5f, -0.3f, 0.f, 1.3f, 0.3f, 0.f, 0.f, 0.f, 0.f, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);

	// �� 1
	floor = AddObject(-17.8f, -2.f, 0.f, 1.f, 4.f, 0.f, 0.f, 0.f, 0.f, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);

	// �� 2
	floor = AddObject(10.5f, -2.f, 0.f, 1.f, 2.f, 0.f, 0.f, 0.f, 0.f, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);

	// �� 3
	floor = AddObject(12.f, 0.8f, 0.f, 1, 2.8, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);


	// �ٴ�4
	floor = AddObject(9.4, 1.2, 0, 0.5, 0.2, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);


	// �ٴ�5
	floor = AddObject(6.9, 0.1, 0, 2.3, 0.3, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);


	// �ٴ�6
	floor = AddObject(2.1, 1.2, 0, 1.2, 0.3, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);

	// ��3
	floor = AddObject(0.2, 2, 0, 0.5, 3, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);


	floor = AddObject(-0.2, 0, 0, 1, 2, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);


	// �ٴ�6
	floor = AddObject(15, -3.5, 0, 10, 0.3, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);


	// ��4
	floor = AddObject(12.5, -1, 0, 0.5, 5, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);

	// �ٴ�6
	floor = AddObject(17.5, -3, 0, 1, 0.3, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);

	// �ٴ� 7
	floor = AddObject(18, -0.8, 0, 2, 1, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);

	// �ٴ� 8
	floor = AddObject(18, -2.5, 0, 1.5, 0.3, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);


	// �ٴ� 9
	floor = AddObject(19, -1.5, 0, 1.5, 0.3, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_END);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);
}
